# configuration file for CNTK ATIS for language understanding tasks

stderr=$LogDir$\ATIS\log
command=LSTM:LSTMTest

type=double

LSTM=[
    # this is the maximum size for the minibatch, since sequence minibatches are really just a single sequence
  # can be considered as the maximum length of a sentence
    action=train
  makeMode=true

#  recurrent networks are trained with minibatch
#  minibatch size, for example in language model, is the number of input words
#  e.g., 6, corresponds to having 6 inputs words from one sentence
#  In the learning process, we split an input sequence into a vector of subsequences of size T_bptt .
    minibatchSize=1000

  # need to be small since models are updated for each minibatch
    traceLevel=1
    # CPU is -1
  deviceId=$DeviceNumber$

    epochSize=486085

# uncomment NDLNetworkBuilder to use NDL
# need to comment out SimpleNetworkBuilder section
#    NDLNetworkBuilder=[
#        networkDescription=$NdlDir$\lstmNDL.txt
#    ]

    SimpleNetworkBuilder=[
      trainingCriterion=crossentropywithsoftmax
      evalCriterion=crossentropywithsoftmax

 #       # default hidden layer activity
      defaultHiddenActivity=0.1

        # randomization range
        initValueScale=1.6

        # first layer, second layer, and output layer size
    layerSizes=195:50:300:300:108
    # the letter stream doesn't support context-dependent inputs
    streamSizes=108:87
    lookupTableOrderSizes=1:3

        rnnType=JOINTCONDITIONALBILSTMSTREAMS
#        rnnType=UNIDIRECTIONALLSTMWITHPASTPREDICTION
        lookupTableOrder=3


       addPrior=false
       addDropoutNodes=false
       applyMeanVarNorm=false
       uniformInit=true
    ]

    # configuration file, base parameters
    SGD=[
    learningRatesPerSample=0.007
        momentumPerMB=0.0

        gradientClippingWithTruncation=true
    clippingThresholdPerSample=5.0

        # maximum number of epochs
      maxEpochs=100

#        gradientcheck=true
        sigFigs=4

        # for information purpose, number of minibatches to report progress
        numMBsToShowResult=1000

        # Whether use AdaGrad
#        gradUpdateType=AdaGrad
        
        # output model path        
        modelPath=$ExpDir$\cntkdebug.dnn

        # if validation shows that the model has no improvement, then do back-up to the previously 
        # estimated model and reduce learning rate
        loadBestModel=true

        # settings for Auto Adjust Learning Rate
        AutoAdjust=[
            # auto learning rate adjustment
          autoAdjustLR=adjustafterepoch
            reduceLearnRateIfImproveLessThan=0
            increaseLearnRateIfImproveMoreThan=1000000000

            # how much learning rate is reduced 
            learnRateDecreaseFactor=0.5

            # if continously improved, can increase learning rate by the following ratio
            learnRateIncreaseFactor=1.0

            numMiniBatch4LRSearch=100
            numPrevLearnRates=5
            numBestSearchEpoch=1
        ]


        dropoutRate=0
    ]

    reader=[
      # reader to use
      readerType=LUSequenceReader

      ioNodeNames=delayedTargetStream:letterInForward
#      ioNodeNames=delayedTargetStream
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=10000

      dataMultiPass=true

      letterInForward=[
        unk="<unk>"
        wordmap=$DataDir$\ltr.map
        file=$DataDir$\s26.01.train_without_oovs
  
        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0:1:2
        randomize=Auto

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=100

        # this node must be exist in the network description
        ltrForward=[
          dim=87
        ]

        #labels sections
        labelInForward=[
          dim=1
          usewordmap=true

          # if having labelDim, this is for output label
          # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsfwd.txt
        labelType=Category
          beginSequence="BOS"
          endSequence="EOS"
          usewordmap=true

          # input word list
          token=$DataDir$\ltr.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsfwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]

      delayedTargetStream=[
        # this stream is used for training phone LM
        unk="<unk>"
        wordmap=$DataDir$\phn.map
        file=$DataDir$\s6.train.phone

        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0
        randomize=Auto

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=100

        # this node must be exist in the network description
        featureDelayedTarget=[
          dim=108
        ]

        labelIn=[
          dim=1
          usewordmap=true

      # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsbwd.txt
        labelType=Category
          beginSequence="OBOS"

          #wildcat match
          endSequence="OEOS"

          usewordmap=true

          # input word list
          token=$DataDir$\phn.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsbwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]
    ]

    cvReader=[
      # reader to use
      readerType=LUSequenceReader

      ioNodeNames=delayedTargetStream:letterInForward
#      ioNodeNames=delayedTargetStream
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=10000

      letterInForward=[
        unk="<unk>"
        wordmap=$DataDir$\ltr.map
        file=$DataDir$\s26.01.dev_without_oovs
  
        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0:1:2
        randomize=None

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=100

        # this node must be exist in the network description
        ltrForward=[
          dim=87
        ]

        #labels sections
        labelInForward=[
          dim=1
          usewordmap=true

          # if having labelDim, this is for output label
          # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsfwd.txt
        labelType=Category
          beginSequence="BOS"
          endSequence="EOS"
          usewordmap=true

          # input word list
          token=$DataDir$\ltr.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsfwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]

      delayedTargetStream=[
        # this stream is used for training phone LM
        unk="<unk>"
        wordmap=$DataDir$\phn.map
        file=$DataDir$\s6.validate.phone

        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0
        randomize=None

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=100

        # this node must be exist in the network description
        featureDelayedTarget=[
          dim=108
        ]

        labelIn=[
          dim=1
          usewordmap=true

      # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsbwd.txt
        labelType=Category
          beginSequence="OBOS"

          #wildcat match
          endSequence="OEOS"

          usewordmap=true

          # input word list
          token=$DataDir$\phn.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsbwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]
    ]
  ]
]

# set output files path
# set the nodes for outputs
# for LSTM
# accuracy:  98.16%; precision:  94.37%; recall:  94.57%; FB1:  94.47
 
LSTMTest=[
    # this is the maximum size for the minibatch, since sequence minibatches are really just a single sequence
  # can be considered as the maximum length of a sentence
  action=beamSearch

# correspond to the number of words/characteres to train in a minibatch
    minibatchSize=1
  # need to be small since models are updated for each minibatch
    traceLevel=1
  deviceId=-1
  epochSize=4430000
  # which is 886 * 5000
  #recurrentLayer=1
    defaultHiddenActivity=0.1

    modelPath=$MdlDir$\cntkdebug.dnn

    # this is the node to evaluate scores
    evalNodeNames=outputs

    # this is the node to output results
    outputNodeNames=outputs

    beamWidth=1
    maxNbrTokens=10

     minibatchSize=1000

    reader=[
      # reader to use
      readerType=LUSequenceReader

      ioNodeNames=delayedTargetStream:letterInForward
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=10000

      letterInForward=[
        unk="<unk>"
        wordmap=$DataDir$\ltr.map
        file=$DataDir$\s01.01.test_letters
  
        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0:1:2
        randomize=None

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=1

        # this node must be exist in the network description
        ltrForward=[
          dim=87
        ]

        #labels sections
        labelInForward=[
          dim=1
          usewordmap=true

          # if having labelDim, this is for output label
          # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsfwd.txt
        labelType=Category
          beginSequence="BOS"
          endSequence="EOS"
          usewordmap=true

          # input word list
          token=$DataDir$\ltr.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsfwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]

      delayedTargetStream=[
        # this stream is used for training phone LM
        unk="<unk>"
        wordmap=$DataDir$\phn.map
        file=$DataDir$\s6.test.phone

        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0
        randomize=None

        TestEncodingForDecoding=false

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=1

        # this node must be exist in the network description
        featureDelayedTarget=[
          dim=108
        ]

        labelIn=[
          dim=1
          usewordmap=true

      # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsbwd.txt
        labelType=Category
          beginSequence="OBOS"

          #wildcat match
          endSequence="OEOS"

          usewordmap=true

          # is a node for proposal generation
          isproposal=true
          proposalSymbolList=$DataDir$\phn.list

          # input word list
          token=$DataDir$\phn.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsbwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]
    ]

    writer=[
        writerType=LUSequenceWriter

        outputs=[
            file=$OutDir$\output.rec.txt
            token=$DataDir$\phn.list
        ]
    ]
]

# change the ordering of test sentences
LSTMTest2=[
    # this is the maximum size for the minibatch, since sequence minibatches are really just a single sequence
  # can be considered as the maximum length of a sentence
  action=beamSearch

# correspond to the number of words/characteres to train in a minibatch
    minibatchSize=1
  # need to be small since models are updated for each minibatch
    traceLevel=1
  deviceId=-1
  epochSize=4430000
  # which is 886 * 5000
  #recurrentLayer=1
    defaultHiddenActivity=0.1

    modelPath=$MdlDir$\cntkdebug.dnn

    # this is the node to evaluate scores
    evalNodeNames=outputs

    # this is the node to output results
    outputNodeNames=outputs

    beamWidth=1
    maxNbrTokens=10

     minibatchSize=1000

    reader=[
      # reader to use
      readerType=LUSequenceReader

      ioNodeNames=delayedTargetStream:letterInForward
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=10000

      letterInForward=[
        unk="<unk>"
        wordmap=$DataDir$\ltr.map
        file=$DataDir$\s30.02.test_letters
  
        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0:1:2
        randomize=None

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=1

        # this node must be exist in the network description
        ltrForward=[
          dim=87
        ]

        #labels sections
        labelInForward=[
          dim=1
          usewordmap=true

          # if having labelDim, this is for output label
          # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsfwd.txt
        labelType=Category
          beginSequence="BOS"
          endSequence="EOS"
          usewordmap=true

          # input word list
          token=$DataDir$\ltr.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsfwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]

      delayedTargetStream=[
        # this stream is used for training phone LM
        unk="<unk>"
        wordmap=$DataDir$\phn.map
        file=$DataDir$\s30.02.test.phone

        #typedef argvector<size_t> intargvector which is not compatible with negative number
        wordContext=0
        randomize=None

        TestEncodingForDecoding=false

        # number of utterances to be allocated for each minibatch
        nbruttsineachrecurrentiter=1

        # this node must be exist in the network description
        featureDelayedTarget=[
          dim=108
        ]

        labelIn=[
          dim=1
          usewordmap=true

      # vocabulary size
          labelDim=10000
        labelMappingFile=$ExpDir$\sentenceLabelsbwd.txt
        labelType=Category
          beginSequence="OBOS"

          #wildcat match
          endSequence="OEOS"

          usewordmap=true

          # is a node for proposal generation
          isproposal=true
          proposalSymbolList=$DataDir$\phn.list

          # input word list
          token=$DataDir$\phn.list

          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          elementSize=4
          sectionType=labels
          mapping=[
            #redefine number of records for this section, since we don't need to save it for each data record
            wrecords=11
            #variable size so use an average string size
            elementSize=10
            sectionType=labelMapping
          ]
          category=[
            dim=11
            #elementSize=sizeof(ElemType) is default
            sectionType=categoryLabels
          ]
        ]

        #labels sections
        # this name must be exist in the network description
        labels=[
          dim=1
        labelType=Category
          beginSequence="OBOS"
          endSequence="OEOS"

          # output token list
          token=$DataDir$\phn.list

          labelMappingFile=$ExpDir$\sentenceLabelsbwd.out.txt
          #### Write definition ####
          # sizeof(unsigned) which is the label index type
          sectionType=labels
          mapping=[
            sectionType=labelMapping
          ]
          category=[
            sectionType=categoryLabels
          ]
        ]
      ]
    ]

    writer=[
        writerType=LUSequenceWriter

        outputs=[
            file=$OutDir$\output.rec.txt
            token=$DataDir$\phn.list
        ]
    ]
]
