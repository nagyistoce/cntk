# configuration file for class based RNN training
ExpFolder=$ExpDir$
ConfigFolder=$ConfigDir$
DataFolder=$DataDir$

stderr=$ExpFolder$
numCPUThreads=4
# command=dumpNodeInfo
#command=train
#command=test
#command=writeWordAndClassInfo
command=writeWordAndClassInfo:train:test
#command=train:test
type=double

DEVICEID=$DEVICE$

NOISE=100
RATE=0.1
VOCABSIZE=2263
CLASSSIZE=50
makeMode=true
TRAINFILE=comments.cntk.train.txt
VALIDFILE=comments.cntk.valid.txt
TESTFILE=comments.cntk.test.txt

#number of threads
nthreads=4

writeWordAndClassInfo=[
    action=writeWordAndClass
    inputFile=$DataFolder$\$TRAINFILE$
	  outputVocabFile=$ExpFolder$\vocab.txt
    outputWord2Cls=$ExpFolder$\word2cls.txt
    outputCls2Index=$ExpFolder$\cls2idx.txt
    vocabSize=$VOCABSIZE$
	  cutoff=2
    nbrClass=$CLASSSIZE$
    printValues=true
]

dumpNodeInfo=[
    action=dumpnode
    modelPath=$ExpFolder$\modelRnnCNTK
    #nodeName=W0
    printValues=true
]

devtest=[action=devtest]

train=[
    action=train
    minibatchSize=10
    traceLevel=0
    deviceId=$DEVICEID$
    epochSize=4430000
    # which is 886 * 5000
    recurrentLayer=1
    defaultHiddenActivity=0.1
    useValidation=true
    rnnType=CLASSLSTM 

    SimpleNetworkBuilder=[
        trainingCriterion=classcrossentropywithsoftmax
        evalCriterion=classcrossentropywithsoftmax
        nodeType=Sigmoid
        initValueScale=6.0
        layerSizes=$VOCABSIZE$:100:200:$VOCABSIZE$
        addPrior=false
        addDropoutNodes=false
        applyMeanVarNorm=false
        uniformInit=true;

        lookupTableOrder=1

        # these are for the class information for class-based language modeling
        vocabSize=$VOCABSIZE$
        nbrClass=$CLASSSIZE$
    ]

    # configuration file, base parameters
    SGD=[
	    makeMode=true
        learningRatesPerSample=$RATE$
        momentumPerMB=0
        gradientClippingWithTruncation=true
        clippingThresholdPerSample=15.0
        maxEpochs=40
        unroll=false
        numMBsToShowResult=2000
        # gradUpdateType=AdaGrad
        gradUpdateType=None
      
        modelPath=$ExpFolder$\modelRnnCNTK
        loadBestModel=true

        # settings for Auto Adjust Learning Rate
        AutoAdjust=[
            # auto learning rate adjustment
            autoAdjustLR=adjustafterepoch
            reduceLearnRateIfImproveLessThan=0.001
            continueReduce=false
            increaseLearnRateIfImproveMoreThan=1000000000
            learnRateDecreaseFactor=0.5
            learnRateIncreaseFactor=1.382
            numMiniBatch4LRSearch=100
            numPrevLearnRates=5
            numBestSearchEpoch=1
        ]

        dropoutRate=0.0
    ]

    reader=[
      readerType=LMSequenceReader
      randomize=None
      nbruttsineachrecurrentiter=10

      # word class info
      wordclass=$ExpFolder$\vocab.txt
      
      #### write definition
      wfile=$ExpFolder$\sequenceSentence.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataFolder$\$TRAINFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1
        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpFolder$\sentenceLabels.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpFolder$\sentenceLabels.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]

    cvReader=[
      # reader to use
      readerType=LMSequenceReader
      randomize=None
      # word class info
      wordclass=$ExpFolder$\vocab.txt

      # if writerType is set, we will cache to a binary file
      # if the binary file exists, we will use it instead of parsing this file
      # writerType=BinaryReader

      #### write definition
      wfile=$ExpFolder$\sequenceSentence.valid.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataFolder$\$VALIDFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      # it should be the same as that in the training set
      labelIn=[
        dim=1

        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpFolder$\sentenceLabels.out.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpFolder$\sentenceLabels.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]
]


test=[
    action=eval

    # correspond to the number of words/characteres to train in a minibatch
    minibatchSize=1
    # need to be small since models are updated for each minibatch
    traceLevel=0
    deviceId=$DEVICEID$
    epochSize=4430000
    # which is 886 * 5000
    recurrentLayer=1
    defaultHiddenActivity=0.1
    useValidation=true
    rnnType=CLASSLSTM

    modelPath=$ExpFolder$\modelRnnCNTK
    evalNodeNames=EvalNodeClassBasedCrossEntrpy

    reader=[
      # reader to use
      readerType=LMSequenceReader
      randomize=None
      # word class info
      wordclass=$ExpFolder$\vocab.txt

      #### write definition
      wfile=$ExpFolder$\sequenceSentence.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      # wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      # windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataFolder$\$TESTFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1

        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpFolder$\sentenceLabels.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="</s>"
        endSequence="</s>"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpFolder$\sentenceLabels.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]
]
