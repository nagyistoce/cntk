
# the configuration file followed after configFile= can be found in 
# setups\

# the data directory pointed out in global.config is 
# DataDir=\\speechstore5\transient\kaishengy\data\smt\cntk
# this can be set to your data path, which you can copy the English-to-French data from 
# in CNTK format from  
http://yunpan.cn/cwctrcp7rKXIK  
password: a4fa

# July 2nd 2015
# dry run data
# data is preprocessed at 
s04.lmdata.sh

Training steps:
steps/s03.class.log 

# use this to do unit test and gradient checking because a subset of training and validation are used
# so that unit test and gradient check can be quickly done
steps/s03.class.smallleraning.rate.txt



# large scale english to french
s05.lmdata.sh

# training 
