# configuration file for CNTK ATIS for language understanding tasks

stderr=$LogDir$\SMT\log
command=writeEncoderWordAndClassInfo:writeDecoderWordAndClassInfo:LSTM

LSTMDIM=$VLSTMDIM$
EMBDIM=$VEMBDIM$
NBRUTT=1

type=double

writeEncoderWordAndClassInfo=[
    action=writeWordAndClass
    inputFile=$DataDir$\$TRAINSRCFILE$
    outputVocabFile=$ExpDir$\vocab.src.txt
    outputWord2Cls=$ExpDir$\word2cls.src.txt
    outputCls2Index=$ExpDir$\cls2idx.src.txt
    vocabSize=$VOCABSIZE$
    cutoff=2
    nbrClass=$CLASSSIZE$
    printValues=true
]

writeDecoderWordAndClassInfo=[
    action=writeWordAndClass
    inputFile=$DataDir$\$TRAINTGTFILE$
    outputVocabFile=$ExpDir$\vocab.tgt.txt
    outputWord2Cls=$ExpDir$\word2cls.tgt.txt
    outputCls2Index=$ExpDir$\cls2idx.tgt.txt
    vocabSize=$VOCABSIZE$
    cutoff=2
    nbrClass=$CLASSSIZE$
    printValues=true
]

LSTM=[
    # this is the maximum size for the minibatch, since sequence minibatches are really just a single sequence
  # can be considered as the maximum length of a sentence
    action=trainEncoderDecoder
  makeMode=true

#  recurrent networks are trained with minibatch
#  minibatch size, for example in language model, is the number of input words
#  e.g., 6, corresponds to having 6 inputs words from one sentence
#  In the learning process, we split an input sequence into a vector of subsequences of size T_bptt .
    minibatchSize=50

  # need to be small since models are updated for each minibatch
    traceLevel=1
    # CPU is -1
  deviceId=$DeviceNumber$

    # for each epoch, maximum number of input words is set below
#    epochSize=12075604 sentences
# nbr of words
    epochSize=3687812

    EncoderNetworkBuilder=[
      trainingCriterion=crossentropywithsoftmax
      evalCriterion=crossentropywithsoftmax

	    sparseinput=true

      defaultHiddenActivity=0.1

        # randomization range
        initValueScale=1.6

        # first layer, second layer, and output layer size
    layerSizes=$VOCABSIZE$:$EMBDIM$:$LSTMDIM$
    # the letter stream doesn't support context-dependent inputs
    streamSizes=$SRCFEATDIM$
    lookupTableOrderSizes=1

        rnnType=LSTMENCODER
        lookupTableOrder=1

       addPrior=false
       addDropoutNodes=false
       applyMeanVarNorm=false
       uniformInit=true
    ]

    DecoderNetworkBuilder=[
      trainingCriterion=ClassCrossEntropyWithSoftmax
      evalCriterion=ClassCrossEntropyWithSoftmax

	  sparseinput=true
	  nbrClass=79
 #       # default hidden layer activity
      defaultHiddenActivity=0.1

        # randomization range
        initValueScale=1.6

        # first layer, second layer, and output layer size
        # the second layer must have the same dimension as the first layer
        # because 40 is matched to the output layer dimension from encoder network
    layerSizes=$VOCABSIZE$:$EMBDIM$:$LSTMDIM$:$VOCABSIZE$
    recurrentLayer=2
    # the letter stream doesn't support context-dependent inputs
    streamSizes=40
    lookupTableOrderSizes=1

        rnnType=ALIGNMENTSIMILARITYGENERATOR
        lookupTableOrder=1

       # these are for the class information for class-based language modeling
        vocabSize=$VOCABSIZE$
        nbrClass=$CLASSSIZE$

       addPrior=false
       addDropoutNodes=false
       applyMeanVarNorm=false
       uniformInit=true
    ]

    # configuration file, base parameters
    SGD=[
    learningRatesPerSample=0.0001
        momentumPerMB=0.0

        gradientClippingWithTruncation=true
    clippingThresholdPerSample=5.0

      # use hidden states for encoder decoder training
      useHiddenStates=true
      encoderNodes="LSTM0"
      decoderNodes="LSTM0"

        # maximum number of epochs
      maxEpochs=100

#        gradientcheck=true
        sigFigs=4

        # for information purpose, number of minibatches to report progress
        numMBsToShowResult=1000

        # Whether use AdaGrad
        #gradUpdateType=AdaGrad

        # output model path
        modelPath=$ExpDir$\smt.lstm

        # if validation shows that the model has no improvement, then do back-up to the previously
        # estimated model and reduce learning rate
        loadBestModel=true

        # settings for Auto Adjust Learning Rate
        AutoAdjust=[
            # auto learning rate adjustment
          autoAdjustLR=adjustafterepoch
            reduceLearnRateIfImproveLessThan=0
            increaseLearnRateIfImproveMoreThan=1000000000

            # how much learning rate is reduced
            learnRateDecreaseFactor=0.5

            # if continously improved, can increase learning rate by the following ratio
            learnRateIncreaseFactor=1.0

            numMiniBatch4LRSearch=100
            numPrevLearnRates=5
            numBestSearchEpoch=1
        ]


        dropoutRate=0
    ]

    encoderReader=[
      readerType=LMSequenceReader
      randomize=None
      nbruttsineachrecurrentiter=$NBRUTT$

      # word class info
      wordclass=$ExpDir$\vocab.src.txt
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.src.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataDir$\$TRAINSRCFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1
        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpDir$\sentenceLabels.src.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpDir$\sentenceLabels.src.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]

    decoderReader=[
      readerType=LMSequenceReader
      randomize=None
      nbruttsineachrecurrentiter=$NBRUTT$

      # word class info
      wordclass=$ExpDir$\vocab.tgt.txt
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.tgt.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataDir$\$TRAINTGTFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1
        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpDir$\sentenceLabels.tgt.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpDir$\sentenceLabels.tgt.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]

    encoderCVReader=[
      readerType=LMSequenceReader
      randomize=None
      nbruttsineachrecurrentiter=$NBRUTT$

      # word class info
      wordclass=$ExpDir$\vocab.src.txt
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.src.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataDir$\$VALIDATESRCFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1
        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpDir$\sentenceLabels.src.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpDir$\sentenceLabels.src.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]

    decoderCVReader=[
      readerType=LMSequenceReader
      randomize=None
      nbruttsineachrecurrentiter=$NBRUTT$

      # word class info
      wordclass=$ExpDir$\vocab.tgt.txt
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.tgt.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataDir$\$VALIDATETGTFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1
        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpDir$\sentenceLabels.tgt.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpDir$\sentenceLabels.tgt.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]
  ]
]

LSTMTest=[
    # this is the maximum size for the minibatch, since sequence minibatches are really just a single sequence
  # can be considered as the maximum length of a sentence
  action=testEncoderDecoder

# correspond to the number of words/characteres to train in a minibatch
    minibatchSize=1
  # need to be small since models are updated for each minibatch
    traceLevel=1
  deviceId=$DeviceNumber$
  epochSize=5000
  # which is 886 * 5000
  #recurrentLayer=1
    defaultHiddenActivity=0.1

    encoderModelPath=$MdlDir$\smt.lstm.encoder
    decoderModelPath=$MdlDir$\smt.lstm.decoder

    # this is the node to evaluate scores
    evalNodeNames=PosteriorProb

    # this is the node to output results
    outputNodeNames=outputs

    beamWidth=1
    maxNbrTokens=10

     minibatchSize=50

    encoderReader=[
      readerType=LMSequenceReader
      randomize=None
      nbruttsineachrecurrentiter=$NBRUTT$

      # word class info
      wordclass=$ExpDir$\vocab.src.txt
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.src.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      file=$DataDir$\$TESTSRCFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1
        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpDir$\sentenceLabels.src.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpDir$\sentenceLabels.src.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]

    decoderReader=[
      readerType=LMSequenceReader
      randomize=None
      nbruttsineachrecurrentiter=$NBRUTT$

      # word class info
      wordclass=$ExpDir$\vocab.tgt.txt
      
      #### write definition
      wfile=$ExpDir$\sequenceSentence.tgt.bin
      #wsize - inital size of the file in MB
      # if calculated size would be bigger, that is used instead
      wsize=256

      unk="<unk>"

      #wrecords - number of records we should allocate space for in the file
      # files cannot be expanded, so this should be large enough. If known modify this element in config before creating file
      wrecords=1000
      #windowSize - number of records we should include in BinaryWriter window
      windowSize=$VOCABSIZE$

      #file=$DataDir$\$TESTTGTFILE$

      #additional features sections
      #for now store as expanded category data (including label in)
      features=[
        # sentence has no features, so need to set dimension to zero
        dim=0
        ### write definition
        sectionType=data
      ]
      # sequence break table, list indexes into sequence records, so we know when a sequence starts/stops
      sequence=[
        dim=1
        wrecords=2
        ### write definition
        sectionType=data
      ]
      #labels sections
      labelIn=[
        dim=1
        # vocabulary size
        labelDim=$VOCABSIZE$
        labelMappingFile=$ExpDir$\sentenceLabels.tgt.txt
        labelType=Category
        beginSequence="</s>"
        endSequence="</s>"

        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=11
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=11
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
      #labels sections
      labels=[
        dim=1
        labelType=NextWord
        beginSequence="O"
        endSequence="O"

        # vocabulary size
        labelDim=$VOCABSIZE$

        labelMappingFile=$ExpDir$\sentenceLabels.tgt.out.txt
        #### Write definition ####
        # sizeof(unsigned) which is the label index type
        elementSize=4
        sectionType=labels
        mapping=[
          #redefine number of records for this section, since we don't need to save it for each data record
          wrecords=3
          #variable size so use an average string size
          elementSize=10
          sectionType=labelMapping
        ]
        category=[
          dim=3
          #elementSize=sizeof(ElemType) is default
          sectionType=categoryLabels
        ]
      ]
    ]

    writer=[
        writerType=LMSequenceWriter

        outputs=[
            file=$OutDir$\output.rec.txt
            token=$ExpDir$\vocab.tgt.txt
        ]
    ]
]

