# s01 
head -1000 bitext_voc80000.fr > bitext_voc80000.1000.fr

head -1000 bitext_voc160000.en > bitext_voc160000.1000.en

# then run the following in Python
# util.convert2ascii('d:/data/smt/bitext_voc80000.1000.fr', 'd:/data/smt/bitext_voc80000.ascii.fr')
# util.convert2ascii('d:/data/smt/bitext_voc160000.1000.en', 'd:/data/smt/bitext_voc160000.ascii.en')

# util.add_silence_ending('d:/data/smt/bitext_voc80000.ascii.fr', 'd:/data/smt/bitext_voc80000.ascii.wsil.fr')
# util.add_silence_ending('d:/data/smt/bitext_voc160000.ascii.en', 'd:/data/smt/bitext_voc160000.ascii.wsil.en')

# CV set
head -1000 ntst1213.fr > ntst1213.1000.fr

head -1000 ntst1213_voc160000.en > ntst1213_voc160000.1000.en

# util.convert2ascii('d:/data/smt/ntst1213.1000.fr', 'd:/data/smt/ntst1213.ascii.fr')
# util.convert2ascii('d:/data/smt/ntst1213_voc160000.1000.en', 'd:/data/smt/ntst1213_voc160000.ascii.en')

# util.add_silence_ending('d:/data/smt/ntst1213.ascii.fr', 'd:/data/smt/ntst1213.ascii.wsil.fr')
# util.add_silence_ending('d:/data/smt/ntst1213_voc160000.ascii.en', 'd:/data/smt/ntst1213_voc160000.ascii.wsil.en')

# test set
head -1000 ntst14.fr > ntst14.1000.fr

head -1000 ntst14_voc160000.en > ntst14_voc160000.1000.en

# util.convert2ascii('d:/data/smt/ntst14.1000.fr', 'd:/data/smt/ntst14.ascii.fr')
# util.convert2ascii('d:/data/smt/ntst14_voc160000.1000.en', 'd:/data/smt/ntst14_voc160000.ascii.en')

# util.add_silence_ending('d:/data/smt/ntst14.ascii.fr', 'd:/data/smt/ntst14.ascii.wsil.fr')
# util.add_silence_ending('d:/data/smt/ntst14_voc160000.ascii.en', 'd:/data/smt/ntst14_voc160000.ascii.wsil.en')

