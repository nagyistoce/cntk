# This is for cs-en translation

util.splitToTwoFiles('d:/data/smt/cs-en/cs-en/train/train.cs-en', 'd:/data/smt/cs-en/cs-en/train/train.cs', 'd:/data/smt/cs-en/cs-en/train/train.en')
util.splitToTwoFiles('d:/data/smt/cs-en/cs-en/dev_and_test/dev2.cs-en', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.en')
util.splitToTwoFiles('d:/data/smt/cs-en/cs-en/dev_and_test/test.cs-en', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.en')

# add silence for begining and ending
util.convert2ascii('d:/data/smt/cs-en/cs-en/train/train.cs', 'd:/data/smt/cs-en/cs-en/train/train.ascii.cs')
util.add_silence_ending('d:/data/smt/cs-en/cs-en/train/train.ascii.cs', 'd:/data/smt/cs-en/cs-en/train/train.ascii.wsil.cs')
# need to convert d:/data/smt/cs-en/cs-en/train/train.en into UTF8 using sublime2 before calling the following functions
util.convert2ascii('d:/data/smt/cs-en/cs-en/train/train.en', 'd:/data/smt/cs-en/cs-en/train/train.ascii.en')
util.add_silence_ending('d:/data/smt/cs-en/cs-en/train/train.ascii.en', 'd:/data/smt/cs-en/cs-en/train/train.ascii.wsil.en')

# CV set
util.convert2ascii('d:/data/smt/cs-en/cs-en/dev_and_test/dev2.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.ascii.cs')
util.add_silence_ending('d:/data/smt/cs-en/cs-en/dev_and_test/dev2.ascii.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.ascii.wsil.cs')
util.convert2ascii('d:/data/smt/cs-en/cs-en/dev_and_test/dev2.en', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.ascii.en')
util.add_silence_ending('d:/data/smt/cs-en/cs-en/dev_and_test/dev2.ascii.en', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.ascii.wsil.en')

# test set
util.convert2ascii('d:/data/smt/cs-en/cs-en/dev_and_test/test.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.ascii.cs')
util.add_silence_ending('d:/data/smt/cs-en/cs-en/dev_and_test/test.ascii.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.ascii.wsil.cs')
util.convert2ascii('d:/data/smt/cs-en/cs-en/dev_and_test/test.en', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.ascii.en')
util.add_silence_ending('d:/data/smt/cs-en/cs-en/dev_and_test/test.ascii.en', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.ascii.wsil.en')

