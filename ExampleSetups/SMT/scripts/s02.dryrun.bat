REM save vocabulary

python d:\tools\GroundHog\experiments\nmt\preprocess\preprocess.py -d ..\dryrun\data\vocab.en.pkl -v 300 -b ..\dryrun\data\binarized_text.en.pkl -p ..\dryrun\data\bitext_voc300.en
python d:\tools\GroundHog\experiments\nmt\preprocess\invert-dict.py ..\dryrun\data\vocab.en.pkl ..\dryrun\data\ivocab.en.pkl
python d:\tools\GroundHog\experiments\nmt\preprocess\convert-pkl2hdf5.py ..\dryrun\data\binarized_text.en.pkl ..\dryrun\data\binarized_text.en.h5

python d:\tools\GroundHog\experiments\nmt\preprocess\preprocess.py -d ..\dryrun\data\vocab.fr.pkl -v 300 -b ..\dryrun\data\binarized_text.fr.pkl -p ..\dryrun\data\bitext_voc300.fr
python d:\tools\GroundHog\experiments\nmt\preprocess\invert-dict.py ..\dryrun\data\vocab.fr.pkl ..\dryrun\data\ivocab.fr.pkl
python d:\tools\GroundHog\experiments\nmt\preprocess\convert-pkl2hdf5.py ..\dryrun\data\binarized_text.fr.pkl ..\dryrun\data\binarized_text.fr.h5
