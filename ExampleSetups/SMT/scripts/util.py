# python scripts

'''
add silence ending to the begining and ending of a sentence 
the silence ending and begining symbol is </s>
example:
util.add_silence_ending('d:/data/smt/bitext_voc80000.ascii.fr', 'd:/data/smt/bitext_voc80000.ascii.wsil.fr')
util.add_silence_ending('d:/data/smt/bitext_voc160000.ascii.en', 'd:/data/smt/bitext_voc160000.ascii.wsil.en')
'''
def add_silence_ending(fn, fnout):
	outfile = open(fnout, 'wt')
	with open(fn) as infile:
		for line in infile:
			line = line.strip()
			newline = '</s> ' + line + ' </s>'
			outfile.write(newline + '\n')

	outfile.close()


'''
convert to ascii file
example:
util.convert2ascii('d:/data/smt/bitext_voc80000.1000.fr', 'd:/data/smt/bitext_voc80000.ascii.fr')
util.convert2ascii('d:/data/smt/bitext_voc160000.1000.en', 'd:/data/smt/bitext_voc160000.ascii.en')
util.convert2ascii('d:/data/smt/bitext_voc160000.1000.en', 'd:/data/smt/bitext_voc160000.ascii.en')
'''
def convert2ascii(fn, fnout):
	import codecs
	of = open(fnout, 'wt')
	with open (fn) as infile:
		for line in infile:
			line = line.strip()
			if len(line) > 0:
				lineu = line.decode('utf8')
				of.write(lineu.encode("ASCII", 'ignore'))
				of.write('\n')

	of.close()

'''
convert to ascii file
example:
util.splitToTwoFiles('d:/data/smt/cs-en/cs-en/train/train.cs-en', 'd:/data/smt/cs-en/cs-en/train/train.cs', 'd:/data/smt/cs-en/cs-en/train/train.en')
util.splitToTwoFiles('d:/data/smt/cs-en/cs-en/dev_and_test/dev2.cs-en', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/dev2.en')
util.splitToTwoFiles('d:/data/smt/cs-en/cs-en/dev_and_test/test.cs-en', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.cs', 'd:/data/smt/cs-en/cs-en/dev_and_test/test.en')
'''
def splitToTwoFiles(fn, fnsrcout, fntgtout):

	ofs = open(fnsrcout, 'wt')
	oft = open(fntgtout, 'wt')
	with open (fn) as infile:
		for line in infile:
			line = line.strip()
			if len(line) > 0:
				idx = line.find('|||')
				src = line[0:idx]
				tgt = line[idx+3:-1]

				ofs.write(src)
				ofs.write('\n')

				oft.write(tgt)
				oft.write('\n')

	ofs.close()
	oft.close()

"""
util.info('d:/data/smt/cs-en/cs-en/train/train.ascii.wsil.cs', 'd:/data/smt/cs-en/cs-en/train/train.cs.words')
util.info('d:/data/smt/cs-en/cs-en/train/train.ascii.wsil.en', 'd:/data/smt/cs-en/cs-en/train/train.en.words')
"""
def info(fn, wordfn):

	of = open(wordfn, 'wt')
	with open (fn) as infile:
		for line in infile:
			line = line.strip()
			if len(line) > 0:

				srcln = line.split()
				for s in srcln:
					s = s.strip()
					if len(s) > 0:
						of.write(s + "\n")

	of.close()

