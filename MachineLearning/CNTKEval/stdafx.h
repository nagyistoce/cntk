// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS // "secure" CRT not available on all platforms
#ifdef _WIN32
#include "targetver.h"
#endif
#include <stdio.h>
#include <math.h>

// TODO: reference additional headers your program requires here
