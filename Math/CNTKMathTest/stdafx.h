//
// <copyright file="stdafx.h" company="Microsoft">
//     Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
//
// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define _CRT_SECURE_NO_WARNINGS // "secure" CRT not available on all platforms
#include "targetver.h"

// Headers for CppUnitTest
#pragma warning (disable: 4505) // 'Microsoft::VisualStudio::CppUnitTestFramework::ToString' : unreferenced local function has been removed
#include "CppUnitTest.h"

// TODO: reference additional headers your program requires here
